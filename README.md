Thema Kruskal-Algorithmus

Team Gizem Yilmaz und Gizem Yami

Funktionsbeschreibung
- Graphentheroie zur Berechnung minimaler Bäume
- ganzzahliges lineares Optimierungsproblem
- optimale Lösung
- sortiert Kanten nach deren Bewertung

Zeitplan
- Grobkonzept während Semesterferien (T2)
- Feinkonzept ende Semesters (T2)

Zusammenarbeit
- 14-tätiges Treffen an der THM
- Dokumentenaustausch per Handy und E-Mail

Dokumentation
- Basierend auf Word
- Sprache Deutsch

Offene Punkte, Fragen
- Umfang der Algorithmen
- optimales Beispiel
